# Get OS image
FROM ubuntu:20.04
# Author
LABEL maintainer="Ashish Kumar <ashishkrb7@gmail.com>"
# Install tools required for project
RUN apt-get -y update
RUN apt-get install python3-pip -y
# Move files to app folder
COPY ./requirements.txt /app/requirements.txt
# You can comment line 11 and 12 and replace it with your python module
COPY ./app.py /app/app.py
COPY ./data.csv /app/data.csv
# Set working directory
WORKDIR /app
# Install python libraries
RUN python3 -m pip install --no-cache-dir --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt
ENTRYPOINT [ "python3", "app.py" ]