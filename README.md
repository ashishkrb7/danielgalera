# 👉 How to use this dockerfile?

1. To build docker in local

    `docker build -t danielgalera:version_0.0.0 .`

2. To pull docker image

    `docker pull registry.gitlab.com/ashishkrb7/danielgalera:version_0.0.0`

3. To run docker image

    > To run on console

        docker run --rm --name upworkdockerfile danielgalera:version_0.0.0

        docker run --rm --name upworkdockerfile registry.gitlab.com/ashishkrb7/danielgalera:version_0.0.0

    > To run in background

        docker run -dit --name upworkdockerfile danielgalera:version_0.0.0

        docker run --dit --name upworkdockerfile registry.gitlab.com/ashishkrb7/danielgalera:version_0.0.0

## Client

Daniel Galera

## ✍️ Developer

[Ashish Kumar](https://www.upwork.com/freelancers/~0195178efffee3692b)
