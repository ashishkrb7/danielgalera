""" 
Deverloped by : Ashish
Reference     : https://facebook.github.io/prophet/docs/quick_start.html
"""
from fbprophet import Prophet

# Python
import pandas as pd
# from prophet import Prophet

df = pd.read_csv('./data.csv')

m = Prophet()
m.fit(df)

future = m.make_future_dataframe(periods=365)

forecast = m.predict(future)
print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())
